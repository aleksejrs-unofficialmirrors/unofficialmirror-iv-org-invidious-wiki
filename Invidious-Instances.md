# Public Invidious Instances

[Uptime History provided by Uptimerobot](https://uptime.invidio.us/)  
[Instances API](https://instances.invidio.us/)

## Official Instances

* [invidio.us](https://invidio.us) 🇺🇸  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidio.us)
* [kgg2m7yk5aybusll.onion](http://kgg2m7yk5aybusll.onion)
* [axqzx4s6s54s32yentfqojs3x5i7faxza6xo3ehd4bzzsg2ii4fv2iid.onion](http://axqzx4s6s54s32yentfqojs3x5i7faxza6xo3ehd4bzzsg2ii4fv2iid.onion)

## List of public Invidious Instances
Note: Please add new instances on the bottom so that every user can see how old and how long an instance is listed here.  
Note²: Please move the instance to the offline list **only** if the instance doesn't serve Invidious anymore. Known errors from Invidious doesn't mean the instance is offline.

* [invidious.snopyta.org](https://invidious.snopyta.org/) 🇫🇮  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.snopyta.org)
* [yewtu.be](https://yewtu.be) 🇫🇷 (dash and downloads enabled)  
  Issuer: Let's Encrypt, [Cryptcheck (better alternative to SSLLabs) Verification](https://cryptcheck.fr/https/yewtu.be)
  - [![Uptime Robot status](https://img.shields.io/uptimerobot/status/m783898765-2a4efa67aa8d1c7be6b1dd9d)](https://status.unixfox.eu/783898765)
* [invidious.ggc-project.de](https://invidious.ggc-project.de) 🇩🇪  (uses cloudflare, allows downloads)  
  Issuer: CloudFlare, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.ggc-project.de)
  - [4l2dgddgsrkf2ous66i6seeyi6etzfgrue332grh2n7madpwopotugyd.onion](http://4l2dgddgsrkf2ous66i6seeyi6etzfgrue332grh2n7madpwopotugyd.onion)
* [yt.maisputain.ovh](https://yt.maisputain.ovh) 🇩🇪  (only IPV6)  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=yt.maisputain.ovh)
* [invidious.13ad.de](https://invidious.13ad.de) 🇩🇪  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.13ad.de)
* [invidious.toot.koeln](https://invidious.toot.koeln) 🇩🇪  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.toot.koeln)
* [invidious.fdn.fr](https://invidious.fdn.fr/) :fr:  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.fdn.fr)
* [watch.nettohikari.com](https://watch.nettohikari.com/) 🇩🇪  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=watch.nettohikari.com)
* [invidiou.site](https://invidiou.site/) 🇺🇸  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidiou.site)
* [yt.iswleuven.be](https://yt.iswleuven.be) 🇧🇪  
  Issuer: GÉANT (previously TERENA), [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=yt.iswleuven.be)

### Tor Onion Services
* [fz253lmuao3strwbfbmx46yu7acac2jz27iwtorgmbqlkurlclmancad.onion](http://fz253lmuao3strwbfbmx46yu7acac2jz27iwtorgmbqlkurlclmancad.onion/)
* [qklhadlycap4cnod.onion](http://qklhadlycap4cnod.onion/)
* [c7hqkpkpemu6e7emz5b4vyz7idjgdvgaaa3dyimmeojqbgpea3xqjoid.onion](http://c7hqkpkpemu6e7emz5b4vyz7idjgdvgaaa3dyimmeojqbgpea3xqjoid.onion)
* [mfqczy4mysscub2s.onion](http://mfqczy4mysscub2s.onion/)
* [sb46cryrbrjsfe4kfeculaj3tpke36ynstjm5wym2yh5ng2nflgyugyd.onion](http://sb46cryrbrjsfe4kfeculaj3tpke36ynstjm5wym2yh5ng2nflgyugyd.onion) - offline

### Eepsite (I2P)
* [owxfohz4kjyv25fvlqilyxast7inivgiktls3th44jhk3ej3i7ya.b32.i2p](http://owxfohz4kjyv25fvlqilyxast7inivgiktls3th44jhk3ej3i7ya.b32.i2p) - offline


### Plain HTTP (without SSL):



### Broken SSL or self-signed:



### Blocked:



### Offline:
* [invidious.kabi.tk](https://invidious.kabi.tk) 🇫🇮  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.kabi.tk)
* [tube.poal.co](https://tube.poal.co) 🇺🇸  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=tube.poal.co)
* [yt.openalgeria.org](https://yt.openalgeria.org) 🇩🇿  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=yt.openalgeria.org)
* [invidious.nixnet.xyz](https://invidious.nixnet.xyz) 🇩🇪  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.nixnet.xyz)
* [vid.wxzm.sx](https://vid.wxzm.sx/) 🇳🇱  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=vid.wxzm.sx)
* [yt.lelux.fi](https://yt.lelux.fi/) 🇫🇷  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=yt.lelux.fi)
* [invidious.zapashcanon.fr](https://invidious.zapashcanon.fr) 🇫🇷 
* [invidious.tozein.com](https://invidious.tozein.com) 🇫🇷  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.tozein.com)
* [invidious.snwmds.net](https://invidious.snwmds.net/) 🇧🇷  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.snwmds.net)
* [invidious.snwmds.org](https://invidious.snwmds.org/) 🇯🇵  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.snwmds.org)
* [invidious.snwmds.com](https://invidious.snwmds.com/) 🇮🇪  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.snwmds.com)
* [invidious.sunsetravens.com](https://invidious.sunsetravens.com/) 🇦🇺  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.sunsetravens.com)
* [invidious.gachirangers.com](https://invidious.gachirangers.com/) 🇮🇳  
  Issuer: Let's Encrypt, [SSLLabs Verification](https://www.ssllabs.com/ssltest/analyze.html?d=invidious.gachirangers.com)